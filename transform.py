from PIL import Image
import os
import torchvision.transforms as transforms
from torchvision.transforms.functional import to_pil_image
import numpy as np

input_dir = "./rawdata"
output_dir = "./data"

os.makedirs(output_dir, exist_ok=True)

mean = np.array([0.485, 0.456, 0.406])
std = np.array([0.229, 0.224, 0.225])

transform = transforms.Compose(
    [
        transforms.Resize((224, 224)),
        transforms.ToTensor(),
        transforms.Normalize(mean=mean, std=std),
    ]
)

for class_dir in os.listdir(input_dir):
    input_class_dir = os.path.join(input_dir, class_dir)
    output_class_dir = os.path.join(output_dir, class_dir)
    os.makedirs(output_class_dir, exist_ok=True)
    if os.path.isdir(input_class_dir):
        for filename in os.listdir(input_class_dir):
            if (
                filename.endswith(".jpg")
                or filename.endswith(".JPG")
                or filename.endswith(".jpeg")
                or filename.endswith(".JPEG")
            ):
                img = Image.open(os.path.join(input_class_dir, filename))
                # apply transformations
                img_transformed = transform(img)
                # convert back to numpy array and un-normalize
                numpy_img = img_transformed.permute(1, 2, 0).numpy() * std + mean
                numpy_img = np.clip(numpy_img, 0, 1)
                # Convert the numpy array to PIL Image
                img_pil = Image.fromarray((numpy_img * 255).astype(np.uint8))
                # Save the image
                img_pil.save(os.path.join(output_class_dir, filename))
