import torch
import torch.nn as nn
from torch.utils.data import random_split, ConcatDataset, DataLoader, Subset
import torchvision
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import os

from tqdm import trange

MODEL_FILE = "model.pth"
TRAIN = bool(int(os.getenv("TRAIN", "0")))
EVAL = bool(int(os.getenv("EVAL", "0")))
IN = os.getenv("IN", None)
COLAB = bool(int(os.getenv("COLAB", "0")))


class CoconutNet:
    # Hyper-parameters
    num_epochs = 8
    learning_rate = 0.001

    def __init__(self):
        self.device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
        print("CUDA: ", torch.cuda.is_available())

        # Image pre-processing
        self.transform = transforms.Compose(
            [
                transforms.RandomHorizontalFlip(),
                transforms.RandomVerticalFlip(),
                transforms.RandomRotation(20),
                transforms.RandomResizedCrop(224),
                transforms.ToTensor(),
                transforms.Normalize(
                    mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225]
                ),
            ]
        )

        # Load the whole dataset
        self.full_dataset = datasets.ImageFolder(root="data/", transform=self.transform)

        # Define a split for train and test data
        train_size = int(0.8 * len(self.full_dataset))  # 80% for training
        test_size = len(self.full_dataset) - train_size  # 20% for testing

        # Split the dataset into training and test datasets
        train_dataset, test_dataset = random_split(
            self.full_dataset, [train_size, test_size]
        )

        # new_test_images = datasets.ImageFolder(root="healthy/", transform=transform)
        # test_dataset = ConcatDataset([test_dataset, new_test_images])

        # Data loaders
        BATCH_SIZE = 512
        NUM_WORKERS = 2 if COLAB else 16
        train_loader = torch.utils.data.DataLoader(
            train_dataset,
            batch_size=BATCH_SIZE,
            shuffle=True,
            num_workers=NUM_WORKERS,
        )
        test_loader = torch.utils.data.DataLoader(
            test_dataset,
            batch_size=BATCH_SIZE,
            shuffle=False,
            num_workers=NUM_WORKERS,
        )

        # Load the pre-trained ResNet
        self.model = torchvision.models.resnet18(
            weights=torchvision.models.ResNet18_Weights.DEFAULT
        )
        # model = torch.hub.load("pytorch/vision:v0.10.0", "resnext50_32x4d", pretrained=True)

        # Finetune top layers of ResNet
        for param in self.model.parameters():
            param.requires_grad = False

        # Replace top layer for finetuning.
        NUM_CLASSES = 6
        self.model.fc = nn.Linear(self.model.fc.in_features, NUM_CLASSES)

        self.model = self.model.to(self.device)

        # Loss and optimizer
        criterion = nn.CrossEntropyLoss()
        optimizer = torch.optim.Adam(self.model.parameters(), lr=self.learning_rate)

        def train():
            total_step = len(train_loader)
            best_loss = float("inf")
            patience = 5
            epochs_no_improve = 0
            for epoch in trange(self.num_epochs):
                for i, (images, labels) in enumerate(train_loader):
                    images = images.to(self.device)
                    labels = labels.to(self.device)

                    # Forward pass
                    outputs = self.model(images)
                    loss = criterion(outputs, labels)

                    # Backward and optimize
                    optimizer.zero_grad()
                    loss.backward()
                    optimizer.step()

                    if (i + 1) % 16 == 0:
                        print(
                            f"Epoch [{epoch+1}/{self.num_epochs}], Step [{i+1}/{total_step}] Loss: {loss.item():.4f}"
                        )

                # Evaluate on the validation set
                with torch.no_grad():
                    self.model.eval()
                    validation_loss = 0
                    for inputs, targets in test_loader:
                        validation_loss += loss.item()

                print(
                    f"{epoch} Validation Loss: {validation_loss:.3f}, Loss: {loss.item():.3f}"
                )
                # Check if the validation loss has improved
                if validation_loss < best_loss:
                    best_loss = validation_loss
                    epochs_no_improve = 0
                else:
                    epochs_no_improve += 1
                    if epochs_no_improve == patience:
                        print("Early stopping!")
                        break

            torch.save(self.model, MODEL_FILE)

        if TRAIN:
            train()
        else:
            self.model = torch.load(MODEL_FILE)

        if EVAL:
            self.model.eval()  # eval mode (batchnorm uses moving mean/variance instead of mini-batch mean/variance)
            with torch.no_grad():
                correct = 0
                total = 0
                for images, labels in test_loader:
                    images = images.to(self.device)
                    labels = labels.to(self.device)
                    outputs = self.model(images)
                    _, predicted = torch.max(outputs.data, 1)
                    total += labels.size(0)
                    correct += (predicted == labels).sum().item()

                print(f"Test Accuracy: {(correct/total):.2%}")


if __name__ == "__main__":
    cn = CoconutNet()
    if IN is not None:
        subset_indices = torch.arange(0, len(cn.full_dataset))
        subset = Subset(cn.full_dataset, subset_indices)
        class_names = subset.dataset.classes
        print(class_names)

        single_image_dataset = datasets.ImageFolder(
            root="test/", transform=cn.transform
        )
        single_image_loader = torch.utils.data.DataLoader(
            single_image_dataset,
            batch_size=1,
            shuffle=False,
            num_workers=16,
        )
        cn.model.eval()
        with torch.no_grad():
            for batch_idx, images in enumerate(single_image_loader):
                images = images[0].to(cn.device)
                outputs = cn.model(images)
                _, predicted = torch.max(outputs.data, 1)

                print(
                    f"{single_image_dataset.samples[batch_idx]} Predicted label: {class_names[predicted.item()]}"
                )
